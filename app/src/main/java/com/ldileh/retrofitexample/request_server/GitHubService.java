package com.ldileh.retrofitexample.request_server;

import com.ldileh.retrofitexample.config.ConfigApp;
import com.ldileh.retrofitexample.model_request.Contributor;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by ldileh on 15/07/16.
 */
public interface GitHubService {
    // receive data as array or list (JSONArray) with, Call<List<your_model>>
    // receive data as object (JSONObject) with, Call<your_model>

    // link : example call to get data with method GET
    @GET("repos/{owner}/{repo}/contributors")
    Call<List<Contributor>> repoContributors(
            @Path("owner") String owner,
            @Path("repo") String repo);

    // example call upload in retrofit 2
    // link : use multipart to start upload file
    // RequestBody a string value (post)
    // MultipartBody.Part a File value (file)
    @Multipart
    @POST("upload")
    Call<ResponseBody> upload(@Part("description") RequestBody description,
                              @Part MultipartBody.Part file);

    // set loggin retrofit using
    // set log receiving data / response from server
    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY);
    OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build();

    // set builder retrofit
    public static final Retrofit connect = new Retrofit.Builder()
            .baseUrl(ConfigApp.url)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
