package com.ldileh.retrofitexample.model_request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ldileh on 15/07/16.
 */
public class Contributor {
    @SerializedName("login")
    private String login;
    @SerializedName("avatar_url")
    private String avatar_url;
    @SerializedName("url")
    private String url;

    public Contributor(String login, String avatar_url, String url){
        this.login = login;
        this.avatar_url = avatar_url;
        this.url = url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public String getLogin() {
        return login;
    }

    public String getUrl() {
        return url;
    }
}
