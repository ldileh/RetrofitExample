package com.ldileh.retrofitexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.ldileh.retrofitexample.adapter.AdapterContributor;
import com.ldileh.retrofitexample.model_request.Contributor;
import com.ldileh.retrofitexample.request_server.GitHubService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();

    private TextView text;
    private Button button;
    private ListView listView;

    private List<Contributor> contributors;
    private AdapterContributor adapterContributor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init view
        init_view();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // request to server
                request_to_server();
            }
        });
    }

    public void init_view(){
        text = (TextView) findViewById(R.id.text);
        button = (Button) findViewById(R.id.button);
        listView = (ListView) findViewById(R.id.listview);
    }

    public void request_to_server(){
        // init Retrofit instance
        GitHubService gitHubService = GitHubService.connect.create(GitHubService.class);

        // init url to get response
        Call<List<Contributor>> call = gitHubService.repoContributors("square", "retrofit");

        // set calback response from server
        call.enqueue(new Callback<List<Contributor>>() {
            @Override
            public void onResponse(Call<List<Contributor>> call, Response<List<Contributor>> response) {
                String http_status = Integer.toString(response.code()); // get http status

                if (response.isSuccessful()){
                    contributors = response.body(); // get list item

                    int number_array = contributors.size();

                    text.setText("Number of item = " + Integer.toString(number_array));

                    // init adapter
                    adapterContributor = new AdapterContributor(getApplicationContext(), contributors);
                    listView.setAdapter(adapterContributor);
                }

                Log.d(TAG, "onResponse: HTTP Status = " + http_status );
            }

            @Override
            public void onFailure(Call<List<Contributor>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }
}
