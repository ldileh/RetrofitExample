package com.ldileh.retrofitexample.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ldileh.retrofitexample.model_request.Contributor;
import com.ldileh.retrofitexample.R;

import java.util.List;

/**
 * Created by ldileh on 15/07/16.
 */
public class AdapterContributor extends BaseAdapter {
    private Context context;
    private List<Contributor> mItems;

    public AdapterContributor(Context context, List<Contributor> mItems){
        this.context = context;
        this.mItems = mItems;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.list_item, null);
        }

        ViewHolder holder = new ViewHolder();

        // number of item. not index
        String number = Integer.toString(position + 1);

        holder.name_login = (TextView) convertView.findViewById(R.id.name_login);
        holder.url = (TextView) convertView.findViewById(R.id.url);
        holder.number = (TextView) convertView.findViewById(R.id.number);

        holder.name_login.setText(mItems.get(position).getLogin());
        holder.url.setText(mItems.get(position).getUrl());
        holder.number.setText(number);

        return convertView;
    }

    static class ViewHolder{
        TextView name_login;
        TextView url;
        TextView number;
    }
}
